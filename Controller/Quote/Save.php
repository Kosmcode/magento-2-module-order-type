<?php

namespace KosmCODE\OrderType\Controller\Quote;

use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;

class Save extends Action
{
    /**
     * @param Context $context
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param OrderTypeRepositoryInterface $orderTypeRepository
     */
    public function __construct(
        protected Context                      $context,
        protected QuoteIdMaskFactory           $quoteIdMaskFactory,
        protected CartRepositoryInterface      $quoteRepository,
        protected OrderTypeRepositoryInterface $orderTypeRepository,
    ) {
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     *
     * @throws NoSuchEntityException
     */
    public function execute(): void
    {
        $post = $this->getRequest()->getPostValue();
        if ($post) {
            $cartId = $post['cartId'];
            $orderType = $post['order_type'];
            $loggin = $post['is_customer'];

            if ($loggin === 'false') {
                $cartId = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id')->getQuoteId();
            }

            $quote = $this->quoteRepository->getActive($cartId);

            if (!$quote->getItemsCount()) {
                throw new NoSuchEntityException(__('Cart %1 doesn\'t contain products', $cartId));
            }

            $orderType = $this->orderTypeRepository->getByType($orderType);

            $quote->setData('order_type_id', $orderType->getOrderTypeId());

            $this->quoteRepository->save($quote);
        }
    }
}
