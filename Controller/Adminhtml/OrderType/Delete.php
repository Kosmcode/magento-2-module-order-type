<?php

namespace KosmCODE\OrderType\Controller\Adminhtml\OrderType;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Delete OrderType controller.
 */
class Delete extends Action implements HttpPostActionInterface, HttpGetActionInterface, RoutePathInterface
{
    public const ADMIN_RESOURCE = 'KosmCODE_OrderType::management';

    /**
     * @param Context $context
     * @param OrderTypeRepositoryInterface $orderTypeRepository
     */
    public function __construct(
        protected readonly Context                      $context,
        protected readonly OrderTypeRepositoryInterface $orderTypeRepository,
    ) {
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute(): ResultInterface
    {
        /** @var ResultInterface $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath(self::INDEX_PATH);

        $orderTypeId = (int)$this->getRequest()->getParam(OrderTypeInterface::ORDER_TYPE_ID);

        try {
            $this->orderTypeRepository->deleteById($orderTypeId);

            $this->messageManager->addSuccessMessage(
                __('You have successfully deleted Order Type entity')
            );
        } catch (CouldNotDeleteException|NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        return $resultRedirect;
    }
}
