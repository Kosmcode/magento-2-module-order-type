<?php

namespace KosmCODE\OrderType\Controller\Adminhtml\OrderType;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * OrderType backend index (list) controller.
 */
class Index extends Action implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'KosmCODE_OrderType::management';

    /**
     * @inheritDoc
     */
    public function execute(): ResultInterface|ResponseInterface
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('KosmCODE_OrderType::management');
        $resultPage->addBreadcrumb(__('Order Type'), __('Order Type'));
        $resultPage->addBreadcrumb(__('Manage Order Types'), __('Manage Order Types'));
        $resultPage->getConfig()->getTitle()->prepend(__('Order Type List'));

        return $resultPage;
    }
}
