<?php

namespace KosmCODE\OrderType\Controller\Adminhtml\OrderType;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;

/**
 * Save OrderType controller action.
 */
class Save extends Action implements HttpPostActionInterface, RoutePathInterface
{
    public const ADMIN_RESOURCE = 'KosmCODE_OrderType::management';

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param OrderTypeRepositoryInterface $orderTypeRepository
     */
    public function __construct(
        protected Context                      $context,
        protected DataPersistorInterface       $dataPersistor,
        protected OrderTypeRepositoryInterface $orderTypeRepository,
    ) {
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute(): ResponseInterface|ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $params = $this->getRequest()->getParams();

        try {
            $orderType = $this->orderTypeRepository
                ->new()
                ->addData($params['general']);

            $this->orderTypeRepository->save($orderType);

            $this->messageManager->addSuccessMessage(
                __('The Order Type data was saved successfully')
            );

            $this->dataPersistor->clear('entity');
        } catch (CouldNotDeleteException|AlreadyExistsException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());

            $this->dataPersistor->set('entity', $params);

            return $resultRedirect->setPath(
                self::EDIT_PATH,
                [
                    OrderTypeInterface::ORDER_TYPE_ID
                    => $this->getRequest()->getParam(OrderTypeInterface::ORDER_TYPE_ID)
                ]
            );
        }

        return $resultRedirect->setPath(self::INDEX_PATH);
    }
}
