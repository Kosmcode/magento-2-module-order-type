<?php

namespace KosmCODE\OrderType\Controller\Adminhtml\OrderType;

interface RoutePathInterface
{
    public const INDEX_PATH = '*/*/';
    public const EDIT_PATH = '*/*/edit';
}
