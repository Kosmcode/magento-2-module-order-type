<?php

namespace KosmCODE\OrderType\Controller\Adminhtml\OrderType;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Edit OrderType entity backend controller.
 */
class Edit extends Action implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'KosmCODE_OrderType::management';

    /**
     * @inheritDoc
     */
    public function execute(): Page|ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('KosmCODE_OrderType::management');

        $resultPage
            ->getConfig()
            ->getTitle()
            ->prepend(__('Edit Order Type'));

        return $resultPage;
    }
}
