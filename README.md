# KosmCODE Magento 2 module order type

Module for Magento 2.x adding Order Type to checkout order

## Tech-Stack

* Created & tested on Magento 2.4
* PHP 8.2

# Setup
1. `composer require kosmcode/module-order-type`
2. `bin/magento module:enable KosmCODE_OrderType`
3. `bin/magento setup:upgrade`
