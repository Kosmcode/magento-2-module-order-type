<?php

namespace KosmCODE\OrderType\Enum;

enum DefaultOrderTypeEnum: string
{
    case standard = 'Standardowe';
    case display = 'Ekspozycyjne';
    case test = 'Testowe';
}
