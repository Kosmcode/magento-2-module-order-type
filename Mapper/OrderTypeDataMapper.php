<?php

namespace KosmCODE\OrderType\Mapper;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Api\Data\OrderTypeInterfaceFactory;
use KosmCODE\OrderType\Model\OrderTypeModel;
use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Converts a collection of OrderType entities to an array of data transfer objects.
 */
class OrderTypeDataMapper
{
    /**
     * @var OrderTypeInterfaceFactory
     */
    private OrderTypeInterfaceFactory $entityDtoFactory;

    /**
     * @param OrderTypeInterfaceFactory $entityDtoFactory
     */
    public function __construct(
        OrderTypeInterfaceFactory $entityDtoFactory
    ) {
        $this->entityDtoFactory = $entityDtoFactory;
    }

    /**
     * Map magento models to DTO array.
     *
     * @param AbstractCollection $collection
     *
     * @return array|OrderTypeInterface[]
     */
    public function map(AbstractCollection $collection): array
    {
        $results = [];

        /** @var OrderTypeModel $item */
        foreach ($collection->getItems() as $item) {

            /** @var OrderTypeInterface|DataObject $entityDto */
            $entityDto = $this->entityDtoFactory->create();
            $entityDto->addData($item->getData());

            $results[] = $entityDto;
        }

        return $results;
    }
}
