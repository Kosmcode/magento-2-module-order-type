<?php

namespace KosmCODE\OrderType\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class SaveOrderTypeObserver implements ObserverInterface
{
    /**
     * @inheritDoc
     *
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer): void
    {
        /** @var Quote $quote */
        $quote = $observer->getData('quote');

        $orderTypeId = $quote->getData('order_type_id');

        if (!$orderTypeId) {
            throw new NoSuchEntityException(__('Order type is not set on quote'));
        }

        /** @var Order $order */
        $order = $observer->getData('order');

        $order->setData('order_type_id', $orderTypeId);
    }
}
