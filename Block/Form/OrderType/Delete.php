<?php

namespace KosmCODE\OrderType\Block\Form\OrderType;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Delete entity button.
 */
class Delete extends GenericButton implements ButtonProviderInterface
{
    /**
     * Retrieve Delete button settings.
     *
     * @return array
     */
    public function getButtonData(): array
    {
        if (!$this->getOrderTypeId()) {
            return [];
        }

        return $this->wrapButtonSettings(
            __('Delete')->getText(),
            'delete',
            sprintf(
                "deleteConfirm('%s', '%s')",
                __('Are you sure you want to delete this Order Type?'),
                $this->getUrl(
                    '*/*/delete',
                    [OrderTypeInterface::ORDER_TYPE_ID => $this->getOrderTypeId()]
                )
            ),
            [],
            20
        );
    }
}
