<?php

namespace KosmCODE\OrderType\Query\OrderType;

use KosmCODE\OrderType\Mapper\OrderTypeDataMapper;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeModel\OrderTypeCollection;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeModel\OrderTypeCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;

/**
 * Get OrderType list by search criteria query.
 */
class GetListQuery
{
    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessor;

    /**
     * @var OrderTypeCollectionFactory
     */
    private OrderTypeCollectionFactory $entityCollectionFactory;

    /**
     * @var OrderTypeDataMapper
     */
    private OrderTypeDataMapper $entityDataMapper;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private SearchResultsInterfaceFactory $searchResultFactory;

    /**
     * @param CollectionProcessorInterface $collectionProcessor
     * @param OrderTypeCollectionFactory $entityCollectionFactory
     * @param OrderTypeDataMapper $entityDataMapper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        CollectionProcessorInterface  $collectionProcessor,
        OrderTypeCollectionFactory    $entityCollectionFactory,
        OrderTypeDataMapper           $entityDataMapper,
        SearchCriteriaBuilder         $searchCriteriaBuilder,
        SearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->collectionProcessor = $collectionProcessor;
        $this->entityCollectionFactory = $entityCollectionFactory;
        $this->entityDataMapper = $entityDataMapper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Get OrderType list by search criteria.
     *
     * @param SearchCriteriaInterface|null $searchCriteria
     *
     * @return SearchResultsInterface
     */
    public function execute(?SearchCriteriaInterface $searchCriteria = null): SearchResultsInterface
    {
        /** @var OrderTypeCollection $collection */
        $collection = $this->entityCollectionFactory->create();

        $searchCriteria = $this->prepareSearchCriteria($searchCriteria, $collection);

        $entityDataObjects = $this->entityDataMapper->map($collection);

        /** @var SearchResultsInterface $searchResult */
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setItems($entityDataObjects);
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);

        return $searchResult;
    }

    /**
     * Method to prepare SearchCriteria
     *
     * @param SearchCriteriaInterface|null $searchCriteria
     * @param OrderTypeCollection $collection
     *
     * @return SearchCriteriaInterface
     */
    protected function prepareSearchCriteria(
        ?SearchCriteriaInterface $searchCriteria,
        OrderTypeCollection $collection
    ): SearchCriteriaInterface {
        if (!$searchCriteria) {
            return $this->searchCriteriaBuilder->create();
        }

        $this->collectionProcessor->process($searchCriteria, $collection);

        return $searchCriteria;
    }
}
