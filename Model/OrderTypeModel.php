<?php

namespace KosmCODE\OrderType\Model;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeResource;
use Magento\Framework\Model\AbstractModel;

class OrderTypeModel extends AbstractModel implements OrderTypeInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'order_type_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(OrderTypeResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getOrderTypeId(): ?int
    {
        return $this->getData(self::ORDER_TYPE_ID) === null
            ? null
            : (int)$this->getData(self::ORDER_TYPE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderTypeId(?int $orderTypeId): self
    {
        $this->setData(self::ORDER_TYPE_ID, $orderTypeId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getType(): ?string
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setType(?string $type): self
    {
        $this->setData(self::TYPE, $type);

        return $this;
    }
}
