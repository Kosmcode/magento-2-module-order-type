<?php

namespace KosmCODE\OrderType\Model\Config\Source;

use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use Magento\Framework\Data\OptionSourceInterface;

readonly class OrderType implements OptionSourceInterface, OptionSourceFrontendInterface
{
    /**
     * @param OrderTypeRepositoryInterface $orderTypeRepository
     */
    public function __construct(
        protected OrderTypeRepositoryInterface $orderTypeRepository,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray(): array
    {
        $orderTypes = $this->orderTypeRepository->getAll();

        $orderTypesOptions = [];

        foreach ($orderTypes->getItems() as $orderType) {
            $orderTypesOptions[] = [
                'value' => $orderType->getOrderTypeId(),
                'label' => $orderType->getType(),
            ];
        }

        return $orderTypesOptions;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArrayForCheckoutFrontend(): array
    {
        $orderTypes = $this->orderTypeRepository->getAll();

        $orderTypesOptions = [];

        foreach ($orderTypes->getItems() as $orderType) {
            $orderTypesOptions[] = [
                'value' => $orderType->getType(),
                'label' => $orderType->getType(),
            ];
        }

        return $orderTypesOptions;
    }
}
