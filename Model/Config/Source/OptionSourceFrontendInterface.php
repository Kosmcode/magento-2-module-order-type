<?php

namespace KosmCODE\OrderType\Model\Config\Source;

interface OptionSourceFrontendInterface
{
    /**
     * Method to get options select array for checkout frontend
     *
     * @return array<int, array>
     */
    public function toOptionArrayForCheckoutFrontend(): array;
}
