<?php

namespace KosmCODE\OrderType\Model\ResourceModel;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderTypeResource extends AbstractDb
{
    /**
     * @var string
     */
    protected string $_eventPrefix = 'order_type_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct(): void
    {
        $this->_init('order_type', OrderTypeInterface::ORDER_TYPE_ID);
        $this->_useIsObjectNew = true;
    }
}
