<?php

namespace KosmCODE\OrderType\Model\ResourceModel\OrderTypeModel;

use KosmCODE\OrderType\Model\OrderTypeModel;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class OrderTypeCollection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'order_type_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct(): void
    {
        $this->_init(OrderTypeModel::class, OrderTypeResource::class);
    }
}
