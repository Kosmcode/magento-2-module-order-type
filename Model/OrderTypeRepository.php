<?php

namespace KosmCODE\OrderType\Model;

use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Api\Data\OrderTypeSearchResultInterfaceFactory;
use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeModel\OrderTypeCollection;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeModel\OrderTypeCollectionFactory;
use KosmCODE\OrderType\Model\ResourceModel\OrderTypeResource;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\NoSuchEntityException;

class OrderTypeRepository implements OrderTypeRepositoryInterface
{
    /**
     * @param OrderTypeModelFactory $orderTypeModelFactory
     * @param OrderTypeCollectionFactory $orderTypeCollectionFactory
     * @param OrderTypeSearchResultInterfaceFactory $orderTypeResultFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchCriteriaInterface $searchCriteria
     * @param OrderTypeResource $orderTypeResource
     */
    public function __construct(
        protected readonly OrderTypeModelFactory                 $orderTypeModelFactory,
        protected readonly OrderTypeCollectionFactory            $orderTypeCollectionFactory,
        protected readonly OrderTypeSearchResultInterfaceFactory $orderTypeResultFactory,
        protected readonly CollectionProcessorInterface          $collectionProcessor,
        protected readonly SearchCriteriaInterface               $searchCriteria,
        protected readonly OrderTypeResource                     $orderTypeResource,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): OrderTypeInterface
    {
        /** @var OrderTypeInterface $orderType */
        $orderType = $this->orderTypeModelFactory->create();
        $this->orderTypeResource->load($orderType, $id);

        if (!$orderType->getOrderTypeId()) {
            throw new NoSuchEntityException(
                __('Unable get OrderType with ID `%1`', $id)
            );
        }

        return $orderType;
    }

    /**
     * @inheritDoc
     */
    public function save(OrderTypeInterface $orderType): OrderTypeInterface
    {
        $this->orderTypeResource->save($orderType);

        return $orderType;
    }

    /**
     * @inheritDoc
     */
    public function delete(OrderTypeInterface $orderType): void
    {
        $this->orderTypeResource->delete($orderType);
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $id): void
    {
        $orderType = $this->getById($id);

        $this->delete($orderType);
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults
    {
        $collection = $this->orderTypeCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, ($collection));

        $searchResults = $this->orderTypeResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function new(): OrderTypeInterface
    {
        return $this->orderTypeModelFactory->create();
    }

    /**
     * @inheritDoc
     */
    public function getAll(): SearchResults
    {
        return $this->getList($this->searchCriteria);
    }

    /**
     * @inheritDoc
     */
    public function getByType(string $type): OrderTypeInterface
    {
        /** @var OrderTypeCollection $orderType */
        $orderType = $this->orderTypeCollectionFactory
            ->create()
            ->addFilter('type', $type)
            ->load();

        if (!$orderType->count()) {
            throw new NoSuchEntityException(
                __('Unable get OrderType by type `%1`', $type)
            );
        }

        return $orderType->getFirstItem();
    }
}
