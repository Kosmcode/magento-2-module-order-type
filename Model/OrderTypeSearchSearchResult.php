<?php

namespace KosmCODE\OrderType\Model;

use KosmCODE\OrderType\Api\Data\OrderTypeSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class OrderTypeSearchSearchResult extends SearchResults implements OrderTypeSearchResultInterface
{

}
