<?php

namespace KosmCODE\OrderType\Setup\Patch\Data;

use KosmCODE\OrderType\Api\OrderTypeRepositoryInterface;
use KosmCODE\OrderType\Enum\DefaultOrderTypeEnum;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddDefaultOrderTypeData implements DataPatchInterface
{
    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param OrderTypeRepositoryInterface $orderTypeRepository
     */
    public function __construct(
        protected ModuleDataSetupInterface     $moduleDataSetup,
        protected EavSetupFactory              $eavSetupFactory,
        protected OrderTypeRepositoryInterface $orderTypeRepository
    ) {
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     *
     * @throws AlreadyExistsException
     */
    public function apply(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        foreach (DefaultOrderTypeEnum::cases() as $defaultOrderTypeEnum) {
            $newOrderType = $this->orderTypeRepository
                ->new()
                ->setType($defaultOrderTypeEnum->value);

            $this->orderTypeRepository->save($newOrderType);
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
