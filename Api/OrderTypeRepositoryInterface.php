<?php

namespace KosmCODE\OrderType\Api;

use Exception;
use KosmCODE\OrderType\Api\Data\OrderTypeInterface;
use KosmCODE\OrderType\Api\Data\OrderTypeSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;

interface OrderTypeRepositoryInterface
{
    /**
     * Method to get OrderType by ID
     *
     * @param int $id
     *
     * @return OrderTypeInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById(int $id): OrderTypeInterface;

    /**
     * Method to save OrderType
     *
     * @param OrderTypeInterface $orderType
     *
     * @return OrderTypeInterface
     *
     * @throws AlreadyExistsException
     */
    public function save(OrderTypeInterface $orderType): OrderTypeInterface;

    /**
     * Method to delete OrderType by object
     *
     * @param OrderTypeInterface $orderType
     *
     * @return void
     *
     * @throws Exception
     */
    public function delete(OrderTypeInterface $orderType): void;

    /**
     * Method to delete OrderType by ID
     *
     * @param int $id
     *
     * @return void
     *
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function deleteById(int $id): void;

    /**
     * Method to get list of OrderType objects
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return OrderTypeSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults;

    /**
     * Method to create new instance of OrderType object
     *
     * @return OrderTypeInterface
     */
    public function new(): OrderTypeInterface;

    /**
     * Method to get list of all OrderType objects
     *
     * @return OrderTypeSearchResultInterface
     */
    public function getAll(): SearchResults;

    /**
     * Method to get OrderType object by type
     *
     * @param string $type
     *
     * @throws NoSuchEntityException
     */
    public function getByType(string $type): OrderTypeInterface;
}
