<?php

namespace KosmCODE\OrderType\Api\Data;

interface OrderTypeSearchResultInterface
{
    /**
     * Method to get items
     *
     * @return OrderTypeInterface[]
     */
    public function getItems();

    /**
     * Method to set items
     *
     * @param OrderTypeInterface[] $items
     *
     * @return void
     */
    public function setItems(array $items);
}
