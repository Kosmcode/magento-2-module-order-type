<?php

namespace KosmCODE\OrderType\Api\Data;

interface OrderTypeInterface
{
    public const ORDER_TYPE_ID = 'order_type_id';
    public const TYPE = 'type';

    /**
     * Getter for OrderTypeId.
     *
     * @return int|null
     */
    public function getOrderTypeId(): ?int;

    /**
     * Setter for OrderTypeId.
     *
     * @param int|null $orderTypeId
     *
     * @return OrderTypeInterface
     */
    public function setOrderTypeId(?int $orderTypeId): self;

    /**
     * Getter for Type.
     *
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * Setter for Type.
     *
     * @param string|null $type
     *
     * @return OrderTypeInterface
     */
    public function setType(?string $type): self;
}
