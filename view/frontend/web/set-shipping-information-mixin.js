/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/url-builder',
    'mage/url',
    'Magento_Checkout/js/model/error-processor',
    'uiRegistry'
], function (
    $,
    wrapper,
    quote,
    customer,
    urlBuilder,
    urlFormatter,
    errorProcessor,
    registry
) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            let isCustomer = customer.isLoggedIn();
            let quoteId = quote.getQuoteId();
            let url = urlFormatter.build('order_type/quote/save');

            let shippingAddress = quote.shippingAddress();

            let attribute = shippingAddress.customAttributes.find(
                function (element) {
                    return element.attribute_code === 'order_type';
                }
            );

            if (!attribute) {
                throw new Error('Order Type parameter is missing');
            }

            if (!attribute.value) {
                throw new Error('Order Type parameter not has value');
            }

            let payload = {
                'cartId': quoteId,
                'order_type': attribute.value,
                'is_customer': isCustomer
            };

            if (!payload.order_type) {
                return true;
            }

            let result = true;

            $.ajax({
                url: url,
                data: payload,
                dataType: 'text',
                type: 'POST',
            }).done(
                function (response) {
                    result = true;
                }
            ).fail(
                function (response) {
                    result = false;
                    errorProcessor.process(response);
                }
            );

            return originalAction();
        });
    };
});
