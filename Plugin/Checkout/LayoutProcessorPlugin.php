<?php

namespace KosmCODE\OrderType\Plugin\Checkout;

use KosmCODE\OrderType\Model\Config\Source\OrderType;
use Magento\Checkout\Block\Checkout\LayoutProcessor;

readonly class LayoutProcessorPlugin
{
    /**
     * @param OrderType $orderTypeSource
     */
    public function __construct(
        protected OrderType $orderTypeSource
    ) {
    }

    /**
     * Plugin after process method
     *
     * @param LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        LayoutProcessor $subject,
        array           $jsLayout
    ): array {
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['order_type']
            = $this->prepareOrderTypeField();

        return $jsLayout;
    }

    /**
     * Method to prepare Order Type field
     *
     * @return array<string, mixed>
     */
    protected function prepareOrderTypeField(): array
    {
        return [
            'component' => 'Magento_Ui/js/form/element/checkbox-set',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/checkbox-set',
                'options' => $this->orderTypeSource->toOptionArrayForCheckoutFrontend(),
                'customEntry' => null,
                'id' => 'order_type'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.order_type',
            'provider' => 'checkoutProvider',
            'label' => __('Order Type'),
            'validation' => [
                'required-entry' => true
            ],
            'visible' => true,
            'id' => 'order_type',
            'sortOrder' => 200,
        ];
    }
}
